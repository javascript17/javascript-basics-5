// let cellphone = {
//     weight: "115 grams",
//     color: "red",
//     ring:function() {
//         return "cellphone is ringing"
//     }
// }

// cellphone.color
// cellphone.ring();

//=============================================

//Pokemon

// let pokemon1 = {
//     name: "Pikachu",
//     level: 3,
//     health: 100,
//     attack: 50,
//     tackle: function() {
//         console.log("Pikachu tackled target")
//     },
//     die: function() {
//         console.log("Pokemon dies.")
//     }
// }

function Pokemon(name) {
    this.name = name;
    this.level = Math.floor(Math.random() * 99 + 1);
    this.health = 100 + (this.level*2);
    this.attack = this.level * 1.25 + 20;
    this.tackle = function() {
        console.log(this.name + " tackled target")
    }
    this.die = function() {
        this.health = 0;
        console.log(this.name + " dies")
    }
}

let pikachu = new Pokemon("Pikachu");
let milotic = new Pokemon("Milotic");
let gyarados = new Pokemon("Gyarados");
let growlithe = new Pokemon("Growlithe");



    