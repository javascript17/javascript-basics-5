function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = 100 + (this.level*2);
    this.attack = this.level * 1.25 + 20;
    this.tackle = function(pokemon) {
        if (this.alive == false) {
            return "This pokemon is dead! Use another one."
        } else {
            if (pokemon.alive == false) {
                return "This pokemon is dead! Killing it is a bit overkill, eh? Choose another one."
            } else {
                console.log(this.name + " tackles " + pokemon.name + "!")
                pokemon.health -= this.attack;
                if (pokemon.health <= 0) {
                    console.log(`${pokemon.name}'s current health: 0`)
                    return pokemon.die();
                } else {
                    return `${pokemon.name}'s health: ${pokemon.health}`;
                }
            }
        }
        this.status = true;
    }
    this.die = function() {
        this.health = 0;
        this.alive = false;
        console.log(this.name + " dies...")
        return "GAME OVER."
    }
}

let geodude = new Pokemon("Geodude", 12);
let eevee = new Pokemon("Eevee", 25);
let arcanine = new Pokemon("Arcanine", 13);
